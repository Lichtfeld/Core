Medieval Core

For Clients, the full contents of this repo should be placed into your game directory.

For Dedicated servers, only place the 7DaysToDieServer_Data, Mods, Data/Configs, Data/Prefabs contents into the game folder.

DO NOT INCLUDE Data/Bundles for dedicated servers, they don't like it.
Also make sure NOT TO COPY the 7DaysToDie_Data resource and asset files into the 7DaysToDieServer_Data or the dedicated server will get angry.
